# Array Methods

## flat
1. Parameter: number (only 1 value), if multiple values are given like **Eg: arr.flat(4,1)** it considers only the first value **4** as parameter, if no value is given by default it takes **1** as parameter.
2. Return: Returns a new Array 
3. Example: 
    ``` javascript
    let arr = [1,2,[3],[[4,5]]];
    arr.flat();
    // [1,2,3,[4,5]]
    let arr = ['a',['b'],'c',[[['d','e']]]];
    arr.flat(2)
    // [ 'a', 'b', 'c', [ 'd', 'e' ] ];
    let arr = ['a',['b'],'c',[[['d','e']]]];
    arr.flat(Infinity);
    // [ 'a', 'b', 'c', 'd', 'e' ]
4. flat accepts a single number value as parameter and returns new array with its sub-array elements concatenated into it upto the specified depth(parameter)
5. It doesn't mutate the original array
   
## push
1. Parameter: n (any) number of values (number, string, boolean, array, null, undefined, object and function etc)
2. Return: Returns array with the values added at the end of array
3. Example:
    ```javascript
    let arr = [1, 2, 3];
    arr.push(4, 5);
    // [ 1, 2, 3, 4, 5]
    let arr = [3,4,22];
    arr.push('a','vd');
    // [ 3, 4, 22, 'a', 'vd']
    let arr = [3,'a',null]
    arr.push(undefined,null,1);
    // [ 3, 'a', null, undefined, null, 1 ]
4. push accepts any number of values as parameters and returns array with the values added into it at the end of array
5. It mutates the original arrray
   
## indexOf
1. Parameter: Accepts a value (number, string, boolean, array, null, undefined, object and function etc)
2. Return: Returns the first index at which the value is found, or -1 if the value is not found.
3. Example:
   ```javascript
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.indexOf(undefined));
   // 3
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.indexOf('c'));
   // -1
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.indexOf(3));
   // 0
4. indexOf method searches for a given value within the array and returns the index of the first occurrence. If the value is not found, it returns -1.
5. It doesn't mutate the original array.
   
## lastIndexOf 
1. Parameter: Parameter: Accepts a value (number, string, boolean, array, null, undefined, object and function etc)
2. Return: Returns the last index at which the value is found, or -1 if the value is not found.
3. Example:
   ```javascript
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.lastIndexOf(undefined));
   // 3
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.lastIndexOf('c'));
   // -1
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.lastIndexOf(null));
   // 4
4. lastIndexOf method searches for a given value within the array and returns the index of the last occurrence. If the value is not found, it returns -1.
5. It doesn't mutate the original array.

## includes
1. Parameter: Accepts a value (number, string, boolean, array, null, undefined, object and function etc)
2. Return: Returns a boolean value
3. Example: 
    ```javascript
    let arr = [3,'a',null,undefined,null,1];
    console.log(arr.includes(null));
    // true
    let arr = [3,'a',null,undefined,null,1];
    console.log(arr.includes(3));
    // true
    let arr = [3,'a',null,undefined,null,1];
    console.log(arr.includes('c'));
    // false
4. includes method checks if a given value exists in the array and returns a boolean indicating the result.
5. It doesn't mutate the original array.

## reverse
1. Parameter: Doesn't accept any parameter
2. Return:  Returns the original array with its elements reversed.
3. Example: 
   ```javascript
   let arr = [1,2,3,4];
   console.log(arr.reverse());
   // [ 4, 3, 2, 1 ]
   let arr = [1,2,3,'a','b'];
   console.log(arr.reverse());
   // [ 'b', 'a', 3, 2, 1 ]
   let arr = [3,'a',null,undefined,null,1];
   console.log(arr.reverse());
   // [ 1, null, undefined, null, 'a', 3 ]
4. reverse method reverses the order of elements in the array.
5. It mutates the original array.
   
## every
1. Parameter: Accepts a callback function
2. Return: Returns a boolean value
3. Example: 
   ```javascript
    let numbers = [2, 4, 6, 8];
    let even = numbers.every(num => num % 2 ===  0); 
    console.log(even);
    // true
    let numbers = [2, 4, 6, 1];
    let even = numbers.every(num => num % 2 ===  0);
    console.log(even);
    // false
    let names = ['Alice', 'Bob', 'Charlie'];
    let big = names.every(name => name.length > 2); 
    console.log(big);
    // true
4. every method tests whether all elements in the array satisfy a given condition given by the callback function.
5. It doesn't mutate the original array.

## shift
1. Parameter: Accepts no parameters.
2. Return: Returns the first element of the array and removes it from the array.
3. Example: 
    ```javascript
    let arr = [1, 2, 3];
    console.log(arr.shift());
    // 1
    let arr = [null, 2, 'a'];
    arr.shift();
    console.log(arr);
    // [ 2, 'a' ]
    let arr = [null, 2, 'a'];
    arr.shift();
    arr.shift();
    console.log(arr);
    // [ 'a' ]
4. shift method removes and returns the first element of the array.
5. It mutates the original array.

## splice
1. Parameter: Accepts a number to start at, a number of elements to remove, and optional values to add.
2. Return: An array containing the changes.
3. Example: 
   ```javascript
    var months = ['Jan', 'March', 'April', 'June'];
    months.splice(1, 0, 'Feb');
    console.log(months);
    // [ 'Jan', 'Feb', 'March', 'April', 'June' ]
    var months = ['Jan', 'March', 'April', 'June'];
    months.splice(2, 0, 'Feb');
    console.log(months);
    // [ 'Jan', 'March', 'Feb', 'April', 'June' ]
    var months = ['Jan', 'March', 'April', 'June'];
    months.splice(1, 1, 'Feb');
    console.log(months);
    // [ 'Jan', 'Feb', 'April', 'June' ]
4. splice method can remove or add elements from/to an array at a given index.
5. It mutates the original array.

## find 
1. Parameter: Accepts a callback function 
2. Return: Returns the first element in the array that satisfies the condition.
3. Example
   ```javascript
    let numbers = [1, 2, 3, 4, 5];
    let even = numbers.find(num => num % 2 === 0); 
    console.log(even);
    // 2
    let numbers = [1, 2, 3, 4, 5];
    let op = numbers.find(num => num > 3); 
    console.log(op);
    // 4
    let numbers = [1, 3, 5];
    let even = numbers.find(num => num % 2 === 0); 
    console.log(even);
    // undefined
4. find method returns the first element in the array that satisfies a given condition given by the callback function.
5. It doesn't mutate the original array.
   
## unshift
1. Parameter: n (any) number of values (number, string, boolean, array, null, undefined, object and function etc)
2. Return: returns array with the values added at beginning of array
3. Example:
    ```javascript
    let arr = [1, 2, 3];
    arr.unshift(4, 5);
    // [ 4, 5, 1, 2, 3 ]
    let arr = [3,4,22];
    arr.unshift('a','vd');
    // [ 'a', 'vd', 3, 4, 22 ]
    let arr = [3,'a',null]
    arr.unshift(undefined,null,1);
    // [ undefined, null, 1, 3, 'a', null ]
4. unshift method adds one or more elements to the beginning of an array and returns the array.
5. It mutates the original array. 

## findIndex 
1. Parameter: Accepts a callback function
2. Return: Returns the index of the first element that satisfies the condition given by the callback or -1 if no element satisfies the condition.
3. Example: 
   ```javascript
    let numbers = [1, 2, 3, 4, 5];
    let even = numbers.findIndex(num => num % 2 === 0); 
    console.log(even);
    // 1
    let numbers = [1, 2, 3, 4, 5];
    let op = numbers.findIndex(num => num > 3); 
    console.log(op);
    // 3
    let numbers = [1, 3, 5];
    let even = numbers.findIndex(num => num % 2 === 0); 
    console.log(even);
    // -1
4. findIndex method returns the index of the first element in the array that satisfies a given condition given by the callback function or -1 if no element satisfies the condition.
5. It doesn't mutate the original array.
   
## filter 
1. Parameter: Accepts a callback function 
2. Return: Returns a new array 
3. Example: 
   ```javascript
    let numbers = [1, 2, 3, 4, 5];
    let even = numbers.filter(num => num % 2 === 0);
    console.log(even);
    // [ 2, 4 ]
    let numbers = [1, 3, 5];
    let even = numbers.filter(num => num % 2 === 0);
    console.log(even);
    // []
    let words = ["van", "vulture", "big"];
    let op = words.filter(word => word[0] === "v");
    console.log(op);
    // [ 'van', 'vulture' ]
4. filter method creates a new array with all elements from the original array that satisfy a given condition given by the callback function.
5. It doesn't mutate the original array.
   
## forEach
1. Parameter: Accepts a callback function 
2. Return: Doesn't return a value
3. Example: 
   ```javascript
    let numbers = [1, 2, 3];
    numbers.forEach(num => console.log(num * 2));
    /*  2
        4
        6 */
    let words = ['va','ba','ca'];
    let changed = words.forEach(word => console.log(word + 'n'));
    /*  van
        ban
        can */
    let numbers = [1, 2, 3];
    console.log(numbers.forEach(num => num * 2));
    // undefined
4. forEach method executes the callback function once for each element in the array.
5. It doesn't mutate the original array.
   
## map
1. Parameter: Accepts a callback function
2. Return: Returns a new array
3. Example:
   ```javascript
    let numbers = [1, 2, 3];
    let double = numbers.map(num => num * 2);
    console.log(double);
    // [ 2, 4, 6 ]
    let numbers = [1, 2, 3];
    let triple = numbers.map(num => num * 3);
    console.log(triple);
    // [ 3, 6, 9 ]
    let words = ['va','ba','ca'];
    let mapped = words.map(word => word + 'n');
    console.log(mapped);
    // [ 'van', 'ban', 'can' ]
4. map method creates a new array by applying the provided callback function to each element in the array
5. It doesn't mutate the original array.

## pop
1. Parameter: Accepts no parameters.
2. Return: Returns the last element of the array and removes it from the array.
3. Example: 
    ```javascript
    let arr = [1, 2, 3];
    console.log(arr.push());
    // 3
    let arr = [null, 2, 'a'];
    arr.push();
    console.log(arr);
    // [ null, 2 ]
    let arr = [null, 2, 'a'];
    arr.push();
    arr.push();
    console.log(arr);
    // [ null ]
4. pop method removes and returns the first element of the array.
5. It mutates the original array.

## reduce
1. Parameter: Accepts a callback function
2. Return: Returns a single value obtained by applying the callback function
3. Example:
   ```javascript
    let numbers = [1, 2, 3, 4, 5];
    let sum = numbers.reduce((acc, num) => acc + num, 0);
    console.log(sum);
    // 15
    let numbers = [1, 2, 3, 4, 5];
    let sum = numbers.reduce((acc, num) => acc + num, 2);
    console.log(sum);
    // 17
    let words = ['Hello', 'world'];
    let concatenated = words.reduce((acc, word) => acc + ' ' + word); 
    // 'Hello world'
4. reduce method applies the callback function to each element in the array, accumulating a single value over each iteration.
5. It doesn't mutate the original array.
   
## slice
1. Parameter: Accepts two values (number)
2. Return: Returns a new array 
3. Example:
   ```javascript
    let numbers = [1, 2, 3, 4, 5];
    let sliced = numbers.slice(1, 3);
    console.log(sliced);
    // [ 2,3 ]
    let numbers = [1, 2, 3, 4, 5];
    let sliced = numbers.slice(2);
    console.log(sliced);
    // [ 3, 4, 5 ]
    let numbers = [1, 2, 3, 4, 5];
    let sliced = numbers.slice(2,-1);
    console.log(sliced);
    // [ 3, 4 ]
4. slice method extracts a part of the array, starting from the specified start index and ending before the specified end index.
5. It doesn't mutate the original array

## some
1. Parameter: Accepts a callback function
2. Return: Returns a boolean value
3. Example: 
   ```javascript
    let numbers = [2, 4, 6, 8];
    let even = numbers.some(num => num % 2 ===  0); 
    console.log(even);
    // true
    let numbers = [2, 4, 6, 1];
    let even = numbers.some(num => num % 2 ===  0);
    console.log(even);
    // true
    let names = ['Alice', 'Bob', 'Charlie'];
    let big = names.every(name => name.length > 2); 
    console.some(big);
    // true
4. some method tests whether some of the elements in the array satisfy a given condition given by the callback function.
5. It doesn't mutate the original array.




    










